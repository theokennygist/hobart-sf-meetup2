import React from 'react'

//IMAGES
import TodoAppImg from 'todo-app.png'
import ErrorSampleReport from 'error-sample-report.png'
import ErrorSampleReport1 from 'error-sample-report1.png'
import SuccessSampleReport from 'success-sample-report.png'
import SuccessSampleReport1 from 'success-sample-report1.png'


export default class TddDemo extends React.Component {
	render() {
		return (
			<div class="container">
				<section class="container text-center body-section">
					<h1 class="blue-color-heading">Heroku React TDD</h1>
					<h3>
						<p>Ensure each component work as expected</p>
						<p>Reduce amount of regression</p>
					</h3>
				</section>
				<section class="body-heading">
					<h3>Goals:</h3>
				</section>
				<section class="body-section">
				<h4>The following are a few of Heroku's key features that I would like to demonstrate:</h4>				
					<ul>
						<li>Test Driven Development (TDD) helps Developers to develop and maintain a more reliable codebase</li>
						<li>Safer to refactor and less bugs</li>
						<li>Reduce Development cost</li>
					</ul>
				</section>
				<section class="body-heading">
					<h3>Application components breakdown:</h3>
				</section>
				<section class="body-section">
					<img src={TodoAppImg} alt="Todo Application" id="todo-app" class="center-block" />
				</section>
				<section class="body-heading">
					<h3>Testing operations:</h3>
				</section>
				<section class="body-section">
					<p class="alert alert-info">Each of the components in the application above, has testing functionality associated with it.</p>
					   <h4>The objectives of this are to ensure: </h4> 
					   <ul>
					   	<li>That each component works as expected.</li>
					   	<li>If additional features are added to the application, 
					   		the existing features will continue to perform as expected</li>
					   </ul>
					
				</section>
				<section class="body-heading">
					<h3>Testing operations - With Error</h3>
				</section>
				<section class="body-section">
					<p class="alert alert-info">NOTE: Below is the example testing report <code>when there is an issue in the component</code></p>
					<img src={ErrorSampleReport} alt="Error report result" id="error-report"/>
					<img src={ErrorSampleReport1} alt="Error report result" id="error-report1" />
					<p class="alert alert-info">The Error report displays which test case is failing. Furthermore, it provides detailed information after the test report 
					</p>
				</section>
				<section class="body-heading">
					<h3>Testing operations - Success</h3>
				</section>
				<section class="body-section">
					<p class="alert alert-info">NOTE: Below is the example success testing report</p>
					<img src={SuccessSampleReport} id="success-report" /><br />
					
					<img src={SuccessSampleReport1} id="success-report1" />
					<p class="alert alert-warning">If you would like to try the Todo application, click this link:  <a href="https://hobart-todo-app.herokuapp.com">HobartTodoApp</a></p>
				</section>		
			</div>
		)
	}
}