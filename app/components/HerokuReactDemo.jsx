import React from 'react'

import Clone from 'clone.png'
import NpmInstall from 'npm-install.png'
import HerokuCreate from 'heroku-create.png'
import HerokuRename from 'rename-heroku.png'
import PushHeroku from 'push-heroku.png'
import HerokuLife from 'heroku-life.png'

export default class HerokuReactDemo extends React.Component {
	render() {
		return (
			<div class="container">
				<section class="container text-center body-section">
					<h1 class="blue-color-heading">Heroku-React Instant Deployment</h1>
					<h3>
						<p class="title-description">Create Bootstrap React application</p>
						<p class="title-description">Deploy into Heroku Instantly</p>
					</h3>
				</section>
				<section class="body-heading">
					<h3>Goals:</h3>
				</section>
				<section class="body-section">	
					<h4>The following are a few of Heroku's key features that I would like to demonstrate:</h4>
						<ul>
							<li>It's easy to use</li>
							<li>It has all packages needed to create React application</li>
							<li>The application can be deployed and goes live instantly</li>
						</ul>										
				</section>
				<section class="body-heading">
					<h3>Prerequisites</h3>
				</section>
				<section class="body-section">					
					<p>Before you begin, ensure you have:</p>
					<ol>
						<li>Heroku Toolbelt installed locally. if not, download and install it through the link below: </li>
						<pre><a href="https://devcenter.heroku.com/articles/heroku-cli">Heroku toolbelt</a></pre><br />
						<li>Ensure you have an heroku account. If not, sign up through the link below:</li>
						<pre><a href="https://signup.heroku.com">Heroku</a></pre>
						<li>Ensure you have git installed locally. If not, download through the link below:</li>
						<pre><a href="https://git-scm.com/downloads" alt="git installer site">Git Installer</a></pre>
					</ol>
				</section>
				<section class="body-heading">
					<h3>Set up Application folder</h3>
				</section>
				<section class="body-section">		
					<ol>
						<li>Clone repo in command prompt as admin</li>
						<pre>git clone git@bitbucket.org:theokennygist/heroku-react.git</pre>
						<img src={Clone} alt="Clone repo" class="heroku-demo"/>
						<li id="clone-li">Once the app has cloned locally, open up the the repo folder</li>
						<li>in that repo folder, open command prompt as admin and run the command below:
							<pre>npm install</pre>
							<img src={NpmInstall} alt="Run npm install" class="heroku-demo" />
						</li>
						<p class="alert alert-info">NOTE: It will download packages into application folder - Heroku-React</p>
						<li>Run the command below:</li>
						<pre>heroku create</pre>
						<img src={HerokuCreate} alt="Create heroku" class="heroku-demo" />
						<p class="alert alert-info">NOTE: Heroku will create an empty application with an auto generated url</p>
						<li>Rename the application by typing in the command below</li>
						<pre>heroku apps:rename hobart-sf</pre>
						<img src={HerokuRename} alt="rename heroku" class="heroku-demo" />
						<p class="alert alert-info">NOTE: hobart-sf is an example. Feel free to choose another application name</p>
						<li>Next, deploy application,Heroku-React, into Heroku by typing the command below:</li>
						<pre>git push heroku master</pre>
						<img src={PushHeroku} alt="Deploy Application" class="heroku-demo" />
						<li>Open the application by typing the command below:</li>
						<pre>heroku open</pre>
						<p class="alert alert-info">NOTE: A prompt will open requiring the user to choose which browser that they would like to use.</p>
						<li>Application will open in browser - like image below:</li>
						<img src={HerokuLife} alt="Heroku-React in browser" class="heroku-demo" />
						<p class="alert alert-info">
							The application goes live instantly on the web. <br />
							if your application name is: hobart-sf <br /> 
							To access it, input the address into your web browser. Eg: https://hobart-sf.herokuapp.com								
						</p>
						<p class="alert alert-warning">Click <code>"Learn more"</code> button to see further information on how to use it. </p>
					</ol>
					
				</section>
			</div>
		)
	}
}