import React from 'react'
import {Link, IndexLink} from 'react-router'

export default class Navigation extends React.Component {
	render() {
		return (
			<nav class="navbar navbar-fixed-top">
				<div class="container">
					<div class="navbar-header">
						<a class="navbar-brand" href="#">Hobart Salesforce Meetup</a>
					</div>
					<div class="navbar-collapse collapse" id="navbar">
						<ul class="nav navbar-nav">
							<li>
								<IndexLink to="/" class="top-link">PPTs</IndexLink>
							</li>	
							<li>
								<Link to="/HerokuReactDemo" class="top-link">Heroku Demo</Link>
							</li>
							<li>
								<Link to="/TddDemo" class="top-link">TDD Demo</Link>
							</li>
							<li>
								<Link to="/Photos" class="top-link">Photos</Link>
							</li>
						</ul>
					</div>
				</div>
			</nav>
		)
	}
}

