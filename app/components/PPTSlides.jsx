import React from 'react'
import ReactDOM from 'react-dom'

//IMAGES - FOR PPTs
import Cover from 'cover.png'
import agenda from 'agenda.png'
import CalvinProfile from 'calvin-profile.png'
import KennyProfile from 'kenny-profile.png'
import About from 'about.png'
import SalesforcePresenter from 'salesforce-presenters.png'
import HerokuCover from 'heroku-cover.png'
import HerokuFeatures from 'heroku-features.png'
import HerokuAddOns from 'heroku-add-ons.png'
import HerokuBuildpacks from 'heroku-buildpacks.png'
import HerokuQuickStart from 'heroku-quickstart.png'
import ReactCover from 'react-cover.png'
import WhyReact from 'why-react.png'
import TopPaying from 'top-paying.png'
import TrendTech from 'trend-tech.png'
import FrontendTopPaying from 'frontend-top-paying.png'
import ReduxFeatures from 'redux-features.png'
import ReactIssue from 'react-issue.png'
import ReduxSolution from 'redux-solution.png'
import WithWithoutRedux from 'with-without-redux.png'
import TddFeatures from 'tdd-features.png'
import AgileIssue from 'agile-issue.png'
import TddCons from 'tdd-cons.png'
import HerokuADdOnsDemo from 'heroku-add-ons-demo.png'
import HerokuReactAppDemo from 'heroku-react-app-demo.png'
import SfEcoUpdate from 'sf-eco-update.png'
import SfEcoUpdate1 from 'sf-eco-update1.png'
import Sponsor from 'sponsors.png'
import Thanks from 'thanks.png'
import References from 'references.png'


export default class PPTSlides extends React.Component {

	render() {
	
	var slides = [agenda, CalvinProfile, KennyProfile, About, SalesforcePresenter, HerokuCover, HerokuFeatures,
			HerokuAddOns,HerokuBuildpacks,HerokuQuickStart,ReactCover,WhyReact,TopPaying,TrendTech,FrontendTopPaying,
			ReduxFeatures,ReactIssue,ReduxSolution, WithWithoutRedux,TddFeatures,AgileIssue, TddCons, HerokuReactAppDemo, 
			SfEcoUpdate, SfEcoUpdate1, Sponsor, Thanks, References];

	var renderSlides = slides.map((slide, index) => {
		return (
			<div key={index} class="item">
				<img src={slide} alt="ppt-slides" class="ppt-slides"/>
			</div>
		)
	});

		return (
			<div class="container">
				<section class="container text-center body-section">
					<h1 class="blue-color-heading">PPT Slides</h1>
					<h3>This page contains 2nd Hobart Salesforce Meetup.</h3>
				</section>

				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">			  
				    
				    <div class="carousel-inner" role="listbox">
				      <div class="item active">
				        <img src={Cover} alt="..." class="ppt-slides"/>				        
				      </div>
				      {renderSlides}
				    </div>
				  
				    
				    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				      <span class="sr-only">Previous</span>
				    </a>
				    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				      <span class="sr-only">Next</span>
				    </a>
				</div>						

			</div> //container
		)
	}
}