import React from 'react'

//PICTURES
import SalesPresentation from 'sales-presentation.jpg'
import Goodies from 'goodies.jpg'
import Audiences from 'audiences.jpg'
import CoHosts from 'co-hosts.jpg'
import CoHosts1 from 'co-hosts1.jpg'

export default class Photos extends React.Component {
	render() {

	var slides = [Goodies, Audiences, CoHosts, CoHosts1];

	var renderSlides = slides.map((slide, index) => {
		return (
			<div key={index} class="item">
				<img src={slide} alt="ppt-slides" class="ppt-slides"/>
			</div>
		)
	});

		return (
			<div class="container">
				<section class="container text-center body-section">
					<h1 class="blue-color-heading">Photos</h1>
					<h3>This page contains photos that are taken during 2nd Hobart Salesforce Meetup.</h3>
				</section>

				<div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="false">			  
				    
				    <div class="carousel-inner" role="listbox">
				      <div class="item active">
				        <img src={SalesPresentation} alt="..." class="ppt-slides"/>				        
				      </div>
				      {renderSlides}
				    </div>
				  
				    
				    <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
				      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				      <span class="sr-only">Previous</span>
				    </a>
				    <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
				      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				      <span class="sr-only">Next</span>
				    </a>
				</div>						

			</div> //container
		)
	}
}