import React from 'react'
import ReactDOM from 'react-dom'
import Bootstrap from 'bootstrap.css'
import Index from 'index.scss'
import {Route, Router, IndexRoute, hashHistory} from 'react-router'

import Main from 'Main'
import HerokuReactDemo from 'HerokuReactDemo'
import PPTSlides from 'PPTSlides'
import TddDemo from 'TddDemo'
import Photos from 'Photos'

export default class App extends React.Component {
	render() {
		return (
			<div class="container">
				<Main />
			</div>
		)
	}
}

const appDOM = document.getElementById('app')
//render() - a function from ReactDOM package
ReactDOM.render(
			<Router history={hashHistory}>
				<Route path="/" component={Main}>
					<Route path="/HerokuReactDemo" component={HerokuReactDemo} />
					<Route path="/TddDemo" component={TddDemo} />
					<Route path="/Photos" component={Photos} />
					<IndexRoute component={PPTSlides} />
				</Route>
			</Router>, 
			appDOM
);